﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Windows;

namespace NurseScheduler
{
    class Patient
    {
        public string name;
        public string city;
        public string location;
        public bool monday, tuesday, wednesday, thursday, friday;
        public string requirement;
        public int weeklyVisit;

        public Patient(string name, string city, string location, bool Monday, bool Tuesday, bool Wednesday, bool Thursday, bool Friday, string requirement, int weeklyVisit)
        {
            this.name = name;
            this.city = city;
            this.location = location;
            monday = Monday;
            tuesday = Tuesday;
            wednesday = Wednesday;
            thursday = Thursday;
            friday = Friday;
            this.requirement = requirement;
            this.weeklyVisit = weeklyVisit;
        }

        public List<string> getRequestedDays() {
            List<string> requestedDays = new List<string>();

            if (monday)
                requestedDays.Add("monday");

            if (tuesday)
                requestedDays.Add("tuesday");

            if (wednesday)
                requestedDays.Add("wednesday");

            if (thursday)
                requestedDays.Add("thursday");

            if (friday)
                requestedDays.Add("friday");

            return requestedDays;
        }
    }

    class Nurse
    {
        public enum NurseType
        {
            RN,
            LVN
        }
        public string name;
        public NurseType nurseType;
        public int dailyMax;
        public List<Nurse> assistants;
        public DateTime lastAssignment;

        public int remainingUses;  //Remaining times nurse can be assigned

        public Nurse(string name, string nurseType, int dailyMax, List<Nurse> assistant, DateTime lastAssignment)
        {
            this.name = name;
            Enum.TryParse(nurseType, out this.nurseType);
            this.dailyMax = dailyMax;
            this.assistants = assistant;
            remainingUses = dailyMax;
            this.lastAssignment = lastAssignment;
        }

    }


    class MainWindow_VM
    {
        //Connection to database
        SQLiteConnection dbConnection;
        SQLiteCommand command;

        public DataTable patientsTable = new DataTable();
        public DataTable nursesTable = new DataTable();
        public DataTable assignmentTable = new DataTable();
        public DataTable scheduleTable = new DataTable();

        List<Patient> patients = new List<Patient>();
        List<Nurse> nurses = new List<Nurse>();

        //Contains the nurse -> patient assignment
        Dictionary<Patient, Nurse> assignmentDictionary = new Dictionary<Patient, Nurse>();

        public string dataBasePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\NurseScheduler";
        public string dataBaseName = "MainDatabase.sqlite";

        public MainWindow_VM()
        {
            DatabaseSetup();
        }

        //Basic Database setup, creating tables etc.
        public string DatabaseSetup()
        {
            try
            {
                if (dbConnection != null && dbConnection.State == ConnectionState.Closed)
                {
                    dbConnection.Close();
                }

                if (!Directory.Exists(dataBasePath))
                {
                    Directory.CreateDirectory(dataBasePath);
                }

                if (!dataBaseName.EndsWith(".sqlite"))
                {
                    dataBaseName += ".sqlite";
                }

                string fullPath = dataBasePath + "\\" + dataBaseName;

                if (!File.Exists(fullPath))
                {
                    SQLiteConnection.CreateFile(fullPath);
                }

                //Connect to database
                dbConnection = new SQLiteConnection("Data Source=" + fullPath + ";Version=3;foreign keys=true");
                dbConnection.Open();

                //Does this table exist?
                command = new SQLiteCommand("SELECT name FROM sqlite_master WHERE type='table' AND name='patients'", dbConnection);
                if (command.ExecuteScalar() == null)
                {
                    //Create patients table
                    DBCommand("CREATE TABLE patients (name TEXT PRIMARY KEY, " +
                                                     "city TEXT, " +
                                                     "location TEXT, " +
                                                     "monday TEXT, " +
                                                     "tuesday TEXT, " +
                                                     "wednesday TEXT, " +
                                                     "thursday TEXT, " +
                                                     "friday TEXT, " +
                                                     "weeklyVisit TEXT, " +
                                                     "requirement TEXT)");
                }
                //Does this table exist?
                command = new SQLiteCommand("SELECT name FROM sqlite_master WHERE type='table' AND name='nurses'", dbConnection);
                if (command.ExecuteScalar() == null)
                {
                    //Create patients table
                    DBCommand("CREATE TABLE nurses (name TEXT PRIMARY KEY, nurseType Text, dailyMax INT, assistant TEXT, lastAssignment TEXT)");
                }

                //Does this table exist?
                command = new SQLiteCommand("SELECT name FROM sqlite_master WHERE type='table' AND name='schedule'", dbConnection);
                if (command.ExecuteScalar() == null)
                {
                    //Create schedule table. It takes over the structure of the patients table and adds external references for nurses for Monday->Friday
                    DBCommand("CREATE TABLE schedule (name TEXT," +
                                                 "mondayNurse TEXT, " +
                                                 "tuesdayNurse TEXT, " +
                                                 "wednesdayNurse TEXT, " +
                                                 "thursdayNurse TEXT, " +
                                                 "fridayNurse TEXT," +
                                                 "FOREIGN KEY (name) REFERENCES patients(name) ON DELETE CASCADE)");
                }

                //Does this table exist?
                command = new SQLiteCommand("SELECT name FROM sqlite_master WHERE type='table' AND name='assignments'", dbConnection);
                if (command.ExecuteScalar() == null)
                {
                    //Create assignment table. Stores association between nurse and patient
                    DBCommand("CREATE TABLE assignments (patientName TEXT," +
                                                 "nurseName TEXT, " +
                                                 "FOREIGN KEY (patientName) REFERENCES patients(name) ON DELETE CASCADE," +
                                                 "FOREIGN KEY (nurseName) REFERENCES nurses(name) ON DELETE CASCADE)");
                }

                return "Database has been successfully loaded";
            }
            catch (Exception e)
            {
                return "Error when loading DB: "+e;
            }
        }

        //General command needed for most SQL uses
        public void DBCommand(string sql)
        {
            command = new SQLiteCommand(sql, dbConnection);
            command.ExecuteNonQuery();
        }

        //Load all the data from the SQL table and populate the DataTable and List references
        public void UpdateDatatable(string commandText, DataTable table)
        {
            try
            {

                using (SQLiteCommand command = new SQLiteCommand(commandText, dbConnection))
                using (SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter(command))
                {
                    table.Clear();
                    dataAdapter.Fill(table);
                }

                //Update the lists based on our new datatable
                if (table == patientsTable)
                {
                    patients = new List<Patient>(table.Rows.Count);
                    foreach (DataRow row in table.Rows)
                    {
                        var values = row.ItemArray;
                        var patient = new Patient(values[0].ToString(), values[1].ToString(), values[2].ToString(), !values[3].ToString().Equals("False"), !values[4].ToString().Equals("False"),
                                                    !values[5].ToString().Equals("False"), !values[6].ToString().Equals("False"), !values[7].ToString().Equals("False"), values[9].ToString(),
                                                    (String.IsNullOrWhiteSpace(values[8].ToString()) ? 0 : Convert.ToInt32(values[8].ToString())));
                        patients.Add(patient);
                    }
                }
                else if (table == nursesTable)
                {
                    nurses = new List<Nurse>(table.Rows.Count);
                    foreach (DataRow row in table.Rows)
                    {
                        var values = row.ItemArray;
                        List<Nurse> assistants = new List<Nurse>();

                        if (values[3] != null)
                        {
                            string[] splitNurses = values[3].ToString().Split(',');
                            foreach (string currentNurse in splitNurses)
                            {
                                assistants.Add(nurses.Find(x => x.name == currentNurse));
                            }
                        }
                        var nurse = new Nurse(values[0].ToString(), values[1].ToString(), (int)values[2], assistants, DateTime.Parse(values[4].ToString()));
                        nurses.Add(nurse);
                    }
                }
                else if (table == assignmentTable)
                {
                    assignmentDictionary.Clear();
                    foreach (DataRow row in table.Rows)
                    {
                        var values = row.ItemArray;
                        if (!assignmentDictionary.ContainsKey(patients.Find(x => x.name == values[0].ToString())))
                        {
                            assignmentDictionary.Add(patients.Find(x => x.name == values[0].ToString()), nurses.Find(x => x.name == values[1].ToString()));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public List<string> getNurseNamesOfType(Nurse.NurseType type)
        {
            List<string> names = new List<string>();
            foreach (Nurse nurse in nurses)
            {
                if (nurse.nurseType == type)
                    names.Add(nurse.name);
            }
            return names;
        }

        public List<string> getPatientsNames() {
            List<string> names = new List<string>();
            foreach (Patient patient in patients)
            {
                names.Add(patient.name);
            }
            return names;
        }

        //Start Schedule Generation
        public void GenerateSchedule()
        {
            //Reset everything to avoid errors
            foreach (Nurse nurse in nurses)
            {
                nurse.remainingUses = nurse.dailyMax;
            }
            DBCommand("DELETE FROM schedule"); //First clear the table, it's easier to not repeat patients this way
            if(assignmentTable.Rows.Count == 0) //Only clear dictionary if no user-defined assignment
            {
                assignmentDictionary.Clear();
            }

            Random r = new Random();
            string lastCity = "";   //Reference to the last city the patient is in, so the same nurse can possibly attend all patients in the same city
            Nurse lastNurse = null; //Reference to last used nurse, used for comparison below
            List<Patient> unassignedPatients = patients.FindAll(x => !assignmentDictionary.ContainsKey(x));
            //Pair every patient with a RN
            unassignedPatients.Sort((x, y) => x.city.CompareTo(y.city));
            for (int i = 0; i < unassignedPatients.Count; i++)
            {
                List<Nurse> stillAvailableNurses = nurses.FindAll(x => x.remainingUses > 0 && x.nurseType != Nurse.NurseType.LVN);   //Select all remaining RNs 
                stillAvailableNurses.Sort((x, y) => x.lastAssignment.Day.CompareTo(y.lastAssignment.Day));
                if (stillAvailableNurses.Count == 0)    //if there are no RNs that are available, then assign LVNs to patients
                {
                    stillAvailableNurses = nurses.FindAll(x => x.nurseType == Nurse.NurseType.LVN && x.remainingUses > 0);
                }

                //Select nurse to use
                if (lastNurse == null || !stillAvailableNurses.Contains(lastNurse) || unassignedPatients[i].city != lastCity || String.IsNullOrWhiteSpace(lastCity))
                {
                    for (int j = 0; j < stillAvailableNurses.Count; j++)
                    {
                        if (stillAvailableNurses[j] != lastNurse)
                        {
                            lastNurse = stillAvailableNurses[j];
                            lastCity = unassignedPatients[i].city;
                            break;
                        }
                    }
                }
                assignmentDictionary.Add(unassignedPatients[i], lastNurse);
                stillAvailableNurses[0].remainingUses--;
            }


            //Resolve visits
           foreach (KeyValuePair<Patient, Nurse> assignment in assignmentDictionary)
            {
                List<string> daysOfWeek = new List<string> { String.Empty, String.Empty, String.Empty, String.Empty, String.Empty };  //Represents days of the week, where 0 is monday and 4 is friday

                if (!String.IsNullOrEmpty(assignment.Key.requirement))
                {
                    int differenceBetweenDay = DateTime.Parse(assignment.Key.requirement).Day - getCurrentMonday().Day;
                    if (differenceBetweenDay >= 0 && differenceBetweenDay < 5)
                    {
                        daysOfWeek[differenceBetweenDay] = assignment.Value.name;
                    }
                }

                List<string> requestedDays = assignment.Key.getRequestedDays(); //Get all checked/requested days from Patient
                int weeklyVisitLeft = assignment.Key.weeklyVisit;
                List<string> chosenDays = new List<string>();   //The list containing the randomly chosen days from requestDays
                while (weeklyVisitLeft > 0)
                {
                    string day = requestedDays[r.Next(requestedDays.Count)];
                    int maxTries = 1000;
                    while (chosenDays.Contains(day) && maxTries > 0) {
                        day = requestedDays[r.Next(requestedDays.Count)];
                        maxTries--;
                    }
                    if (maxTries > 0)
                    {
                        chosenDays.Add(day);
                        weeklyVisitLeft--;
                    }
                }
                int indexOfLastAssistantAssigned = 0;   //Stores the index of the assistant that was assigned last time. This is used so that the LVNs are spread out evenly.
                foreach (string day in chosenDays)
                {
                    switch (day)    //Check which day is currently selected. Then check if its neighbouring days are filled in yet. If not, then assign the RN. If yes, then assign a LVN
                    {
                        case "monday":
                            if (daysOfWeek[1] != assignment.Value.name)
                            {
                                daysOfWeek[0] = assignment.Value.name;
                            }
                            else
                            {
                                if (assignment.Value.assistants != null)
                                    daysOfWeek[0] = assignment.Value.assistants[indexOfLastAssistantAssigned].name;
                                
                            }
                            break;
                        case "tuesday":
                            if (daysOfWeek[0] != assignment.Value.name && daysOfWeek[2] != assignment.Value.name)
                            {
                                daysOfWeek[1] = assignment.Value.name;
                            }
                            else
                            {
                                if (assignment.Value.assistants != null)
                                    daysOfWeek[1] = assignment.Value.assistants[indexOfLastAssistantAssigned].name;
                            }
                            break;
                        case "wednesday":
                            if (daysOfWeek[1] != assignment.Value.name && daysOfWeek[3] != assignment.Value.name)
                            {
                                daysOfWeek[2] = assignment.Value.name;
                            }
                            else
                            {
                                if (assignment.Value.assistants != null)
                                    daysOfWeek[2] = assignment.Value.assistants[indexOfLastAssistantAssigned].name;
                            }
                            break;
                        case "thursday":
                            if (daysOfWeek[2] != assignment.Value.name && daysOfWeek[4] != assignment.Value.name)
                            {
                                daysOfWeek[3] = assignment.Value.name;
                            }
                            else
                            {
                                if (assignment.Value.assistants != null)
                                    daysOfWeek[3] = assignment.Value.assistants[indexOfLastAssistantAssigned].name;
                            }
                            break;
                        case "friday":
                            if (daysOfWeek[3] != assignment.Value.name)
                            {
                                daysOfWeek[4] = assignment.Value.name;
                            }
                            else
                            {
                                if (assignment.Value.assistants != null)
                                    daysOfWeek[4] = assignment.Value.assistants[indexOfLastAssistantAssigned].name;
                            }
                            break;
                    }
                    indexOfLastAssistantAssigned = (indexOfLastAssistantAssigned+1)% assignment.Value.assistants.Count;
                    
                }

                DBCommand("INSERT INTO schedule VALUES ('" + assignment.Key.name + "','" + daysOfWeek[0] + "','" + daysOfWeek[1] + "','" + daysOfWeek[2] + "','" + daysOfWeek[3] + "','" + daysOfWeek[4] + "')");
            }
        }

        public DateTime getCurrentMonday()
        {
            //Find current week's Monday
            DateTime input = DateTime.Now;
            int delta = DayOfWeek.Monday - input.DayOfWeek;
            if (delta > 0)
                delta -= 7;
            return input.AddDays(delta);
        }

        public void ExportAllToCSV(string filePath)
        {
            ExportTableToCSV(patientsTable, filePath);
            ExportTableToCSV(nursesTable, filePath);
            ExportTableToCSV(scheduleTable, filePath);
        }

        void ExportTableToCSV(DataTable dt, string strFilePath)
        {
            // Create the CSV file to which grid data will be exported.
            StreamWriter sw = new StreamWriter(strFilePath, true);

            // First will write the headers.

            int iColCount = dt.Columns.Count;

            for (int i = 0; i < iColCount; i++)
            {
                sw.Write(dt.Columns[i]);
                if (i < iColCount - 1)
                {
                    sw.Write(",");
                }
            }

            sw.Write(sw.NewLine);

            // Now write all the rows.

            foreach (DataRow dr in dt.Rows)
            {
                for (int i = 0; i < iColCount; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        sw.Write(dr[i].ToString());
                    }

                    if (i < iColCount - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
            }
            sw.Write(sw.NewLine);
            sw.Write(sw.NewLine);
            sw.Close();
        }

        public bool isThisLVNReferenced(string lvnName)
        {
            bool returnV = false;
            foreach (Nurse nurse in nurses)
            {
                if (nurse.assistants.Count > 0 && nurse.assistants.Find(x => x != null && x.name == lvnName) != null)
                    returnV = true;
            }
            return returnV;
        }

        public bool isPatientAlreadyAssigned(string patientName)
        {
            foreach (Patient p in assignmentDictionary.Keys)
            {
                if (p.name == patientName)
                    return true;
            }
            return false;
        }
    }
}
