﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace NurseScheduler
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        MainWindow_VM vm;

        public MainWindow()
        {
            InitializeComponent();
            Loaded += delegate { DataContext = new MainWindow_VM(); };

            nurseTypeComboBox.ItemsSource = new List<string> {"RN", "LVN" };
            nurseLastAssignmentPicker.SelectedDate = DateTime.Now.Date;

            DataContextChanged += dataContextSet;
        }

        void dataContextSet(object sender, DependencyPropertyChangedEventArgs e) {
            vm = (MainWindow_VM)this.DataContext;
            databaseFolderTextBox.Text = vm.dataBasePath;
            databaseFileTextBox.Text = vm.dataBaseName;
            updatePatientsView();
            updateNursesView();
            updateAssignmentView();
        }

        #region Patient Tab
        private void patientsAddButton_Click(object sender, RoutedEventArgs e)
        {
            
            //This isn't the safest method (SQL injection problem), but since it's a private software, it isn't that horrible
            vm.DBCommand("INSERT OR REPLACE INTO patients VALUES ('" + patientNameTextBox.Text+"','"+patientCityTextBox.Text+"','"+patientLocationTextBox.Text+"','"+(MondayCheckbox.IsChecked ?? false)+ "','"
                                                                      + (TuesdayCheckbox.IsChecked ?? false) + "','" + (WednesdayCheckbox.IsChecked ?? false) + "','" + (ThursdayCheckbox.IsChecked ?? false) + "','"
                                                                      + (FridayCheckbox.IsChecked ?? false) + "','" + patientWeeklyVisitTextBox.Text + "','" + patientRequirementPicker.Text+"')");
            updatePatientsView();
        }

        private void patientsDeleteSelectedButton_Click(object sender, RoutedEventArgs e)
        {
            
            if (dataGridPatients.SelectedItems.Count > 0)
            {
                foreach (DataRowView row in dataGridPatients.SelectedItems)
                {
                    vm.DBCommand("DELETE FROM patients WHERE name='" + row["name"] +
                                                       "' AND city='" + row["city"] +
                                                       "' AND location='" + row["location"] +
                                                       "' AND requirement='" + row["requirement"] + "'");
                }
            }
            updatePatientsView();
            updateScheduleView();   //Since most probably one of the rows cascade-d
            updateAssignmentView();
        }

        private void patientsDeleteAllButton_Click(object sender, RoutedEventArgs e)
        {
            
            vm.DBCommand("DELETE FROM patients");
            updatePatientsView();
            updateScheduleView();   //Since most probably one of the rows cascade-d
            updateAssignmentView();
        }

        private void patientRequirementPicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            
            if (patientRequirementPicker.SelectedDate != null)
            {

                DateTime day = (DateTime)   patientRequirementPicker.SelectedDate;
                bool isMonday = (MondayCheckbox.IsChecked ?? false) && day.DayOfWeek == DayOfWeek.Monday;   //Here is what ?? does: https://docs.microsoft.com/en-us/dotnet/articles/csharp/language-reference/operators/null-conditional-operator
                bool isTuesday = (TuesdayCheckbox.IsChecked ?? false) && day.DayOfWeek == DayOfWeek.Tuesday;
                bool isWednesday = (WednesdayCheckbox.IsChecked ?? false) && day.DayOfWeek == DayOfWeek.Wednesday;
                bool isThursday = (ThursdayCheckbox.IsChecked ?? false) && day.DayOfWeek == DayOfWeek.Thursday;
                bool isFriday = (FridayCheckbox.IsChecked ?? false) && day.DayOfWeek == DayOfWeek.Friday;

                //If the patient request is MWF but the selected requirement isn't either a Monday, Wednesday or Friday, then default to Monday
                if (!(isMonday || isTuesday || isWednesday || isThursday || isFriday))
                {
                    patientRequirementPicker.SelectedDate = null;
                    infoLabel.Content = "Requirement didn't match up with Request. Please select a valid date!";
                }
            }       
        }

        private void DataGridRow_MouseLeftButtonUp_Patient(object sender, MouseButtonEventArgs e)
        {
            if (dataGridPatients.SelectedItem != null)
            {
                DataRowView row = dataGridPatients.SelectedItem as DataRowView;
                patientNameTextBox.Text = row["name"].ToString();
                patientCityTextBox.Text = row["city"].ToString();
                patientLocationTextBox.Text = row["location"].ToString();
                MondayCheckbox.IsChecked = !(row["monday"].Equals("False"));
                TuesdayCheckbox.IsChecked = !(row["tuesday"].Equals("False"));
                WednesdayCheckbox.IsChecked = !(row["wednesday"].Equals("False"));
                ThursdayCheckbox.IsChecked = !(row["thursday"].Equals("False"));
                FridayCheckbox.IsChecked = !(row["friday"].Equals("False"));
                patientWeeklyVisitTextBox.Text = row["weeklyVisit"].ToString();
                if (!String.IsNullOrEmpty(row["requirement"].ToString()))
                    patientRequirementPicker.SelectedDate = DateTime.Parse(row["requirement"].ToString());
            }
        }

        private void patientWeeklyVisitTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            
            string s = Regex.Replace(((TextBox)sender).Text, @"[^\d]", "");
            if (!String.IsNullOrEmpty(s))
            {
                int amountOfDaysSelected = 0;
                if (MondayCheckbox.IsChecked ?? false)
                    amountOfDaysSelected++;

                if (TuesdayCheckbox.IsChecked ?? false)
                    amountOfDaysSelected++;

                if (WednesdayCheckbox.IsChecked ?? false)
                    amountOfDaysSelected++;

                if (ThursdayCheckbox.IsChecked ?? false)
                    amountOfDaysSelected++;

                if (FridayCheckbox.IsChecked ?? false)
                    amountOfDaysSelected++;

                if (Convert.ToInt32(s) > amountOfDaysSelected)  //The user may only enter weekly visits equal or below the amount of requested days ticked
                {
                    ((TextBox)sender).Text = "0";
                }
                else
                {
                    ((TextBox)sender).Text = s;
                }
            }
        }
        #endregion

        #region Sync
        private void updatePatientsView()
        {
            vm.UpdateDatatable("SELECT * FROM patients", vm.patientsTable);
            dataGridPatients.ItemsSource = vm.patientsTable.DefaultView;
            patientNameComboBox_Assignment.ItemsSource = vm.getPatientsNames();
        }

        private void updateNursesView()
        {
            vm.UpdateDatatable("SELECT * FROM nurses", vm.nursesTable);
            dataGridNurses.ItemsSource = vm.nursesTable.DefaultView;
            assignToAssistantList();
            nurseNameComboBox_Assignment.ItemsSource = vm.getNurseNamesOfType(Nurse.NurseType.RN);
        }

        private void updateScheduleView()
        {
            vm.UpdateDatatable("SELECT * FROM patients NATURAL JOIN schedule", vm.scheduleTable);
            dataGridSchedule.ItemsSource = vm.scheduleTable.DefaultView;
        }

        private void updateAssignmentView()
        {
            vm.UpdateDatatable("SELECT * FROM assignments", vm.assignmentTable);
            dataGridAssignment.ItemsSource = vm.assignmentTable.DefaultView;
        }
        #endregion

        #region Nurse Tab
        private void assignToAssistantList(List<string> activeNurseNames = null)
        {
            List<string> nurseNames = vm.getNurseNamesOfType(Nurse.NurseType.LVN);
            nurseAssistantListView.ItemsSource = null;
            List<CheckBox> checkboxes = new List<CheckBox>();
            foreach (string nurse in nurseNames)
            {
                CheckBox cb = new CheckBox();
                cb.Content = nurse;
                if (activeNurseNames != null && activeNurseNames.Contains(nurse))
                    cb.IsChecked = true;

                checkboxes.Add(cb);
            }
            nurseAssistantListView.ItemsSource = checkboxes;
            if (nurseAssistantListView.Items.Count > 0)
            {
                CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(nurseAssistantListView.ItemsSource);
                view.Filter = assistantFilter;
            }
        }

        private bool assistantFilter(object item)
        {
            if (String.IsNullOrEmpty(nurseAssistantListFilter.Text))
                return true;
            else
                return ((item as CheckBox).Content.ToString().IndexOf(nurseAssistantListFilter.Text, StringComparison.OrdinalIgnoreCase) >= 0);
        }

        private void nurseAssistantListFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(nurseAssistantListView.ItemsSource).Refresh();
        }

        private string getAllAssistants()
        {
            string returnS = "";
            foreach (CheckBox item in nurseAssistantListView.Items)
            {
                if (item.IsChecked ?? false)
                {
                    returnS += item.Content + ",";
                }
            }
            if(returnS.Length > 0)
                returnS = returnS.Remove(returnS.Length - 1);   //Remove last ',' so it doesn't contain an empty nurse

            return returnS;
        }

        private void addNurseButton_Click(object sender, RoutedEventArgs e)
        {

            //This isn't the safest method (SQL injection problem), but since it's a private software, it isn't that horrible 
            vm.DBCommand("INSERT OR REPLACE INTO nurses VALUES ('" + nurseNameTextBox.Text + "','" + nurseTypeComboBox.Text +"','"+nurseDailyMaxTextBox.Text+ "' , '"+getAllAssistants()+"' , '"+nurseLastAssignmentPicker.SelectedDate+"')");
            updateNursesView();
        }

        private void nurseDeleteSelectedButton_Click(object sender, RoutedEventArgs e)
        {
            
            if (dataGridNurses.SelectedItems.Count > 0)
            {
                foreach (DataRowView row in dataGridNurses.SelectedItems)
                {
                    if (vm.isThisLVNReferenced(row["name"].ToString()))
                    {
                        infoLabel.Content = row["name"] + " is referenced by one or more other nurse entries. Please modify/delete them first to remove this entry.";
                    }
                    else
                    {
                        vm.DBCommand("DELETE FROM nurses WHERE name='" + row["name"] +
                                                   "' AND nurseType='" + row["nurseType"] +
                                                   "' AND dailyMax ='" + (int)row["dailyMax"] +
                                                   "' AND assistant='" + row["assistant"] + "'");
                    }
                }
            }
            updateNursesView();
            updateAssignmentView();
        }

        private void nurseDeleteAllButton_Click(object sender, RoutedEventArgs e)
        {
            
            vm.DBCommand("DELETE FROM nurses");
            updateNursesView();
            updateAssignmentView();
        }
        //Allow only numbers in textbox
        private void nurseDailyMaxTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string s = Regex.Replace(((TextBox)sender).Text, @"[^\d]", "");
            ((TextBox)sender).Text = s;
        }

        //Change daily max to correspond to the nurse type for better UX
        private void nurseTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (nurseTypeComboBox.SelectedItem)
            {
                case "RN":
                    nurseDailyMaxTextBox.Text = "4";
                    break;
                case "LVN":
                    nurseDailyMaxTextBox.Text = "5";
                    break;
            }

        }

        private void DataGridRow_MouseLeftButtonUp_Nurse(object sender, MouseButtonEventArgs e)
        {
            if (dataGridNurses.SelectedItem != null)
            {
                DataRowView row = dataGridNurses.SelectedItem as DataRowView;
                nurseNameTextBox.Text = row["name"].ToString();
                nurseTypeComboBox.SelectedItem = row["nurseType"].ToString();
                nurseDailyMaxTextBox.Text = row["dailyMax"].ToString();
                string[] assistants = row["assistant"].ToString().Split(',');
                assignToAssistantList(assistants.ToList());
                if (!String.IsNullOrEmpty(row["lastAssignment"].ToString()))
                    nurseLastAssignmentPicker.SelectedDate = DateTime.Parse(row["lastAssignment"].ToString());
            }
            else
            {
                updateNursesView();
            }
        }

        #endregion

        private void generateSchedule_Click(object sender, RoutedEventArgs e)
        {
            
            vm.GenerateSchedule();
            updateScheduleView();
            updateNursesView();
        }

        private void exportToCSVButton_Click(object sender, RoutedEventArgs e)
        {
            // Create save dialog 
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();

            dlg.DefaultExt = ".csv";
            dlg.Filter = "CSV File (*.csv)|*.csv";
            dlg.AddExtension = true;
            dlg.FileName = "NurseScheduler-Export";

            //Show dialog window
            bool? result = dlg.ShowDialog();


            //Once save was clicked, do the exporting
            if (result == true)
            {
                vm.ExportAllToCSV(dlg.FileName);
                infoLabel.Content = "Tables have been successfully exported to CSV";
            }
        }

        private void databaseLoadButton_Click(object sender, RoutedEventArgs e)
        {
            vm.dataBasePath = databaseFolderTextBox.Text;
            vm.dataBaseName = databaseFileTextBox.Text;
            infoLabel.Content = vm.DatabaseSetup();
            updateNursesView();
            updatePatientsView();
        }
        #region Assignments
        private void addAssignmentButton_Click(object sender, RoutedEventArgs e)
        {
            if (patientNameComboBox_Assignment.SelectedItem != null && nurseNameComboBox_Assignment.SelectedItem != null && !vm.isPatientAlreadyAssigned(patientNameComboBox_Assignment.SelectedItem.ToString()))
            {
                vm.DBCommand("INSERT OR REPLACE INTO assignments VALUES ('" + patientNameComboBox_Assignment.SelectedItem + "','" + nurseNameComboBox_Assignment.SelectedItem + "')");
            }
            updateAssignmentView();
        }

        private void assignmentDeleteSelectedButton_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridAssignment.SelectedItems.Count > 0)
            {
                foreach (DataRowView row in dataGridAssignment.SelectedItems)
                {
                    vm.DBCommand("DELETE FROM assignments WHERE patientName='" + row["patientName"] +
                                                       "' AND nurseName='" + row["nurseName"] + "'");
                }
            }
            updateAssignmentView();
        }

        private void assignmentDeleteAllButton_Click(object sender, RoutedEventArgs e)
        {
            vm.DBCommand("DELETE FROM assignments");
            updateAssignmentView();
        }
        #endregion
    }
}
